FROM quay.io/thallian/alpine-s6:latest

ENV VERSION v1.92_20161226_9000

RUN addgroup -g 2222 yacy
RUN adduser -h /var/lib/yacy -u 2222 -D -G yacy yacy

RUN apk add --no-cache wget libressl openjdk8

RUN wget -qO- http://yacy.net/release/yacy_$VERSION.tar.gz | tar -xz -C /var/lib/yacy --strip 1

ADD /rootfs /

EXPOSE 8090
VOLUME /var/lib/yacy/DATA
