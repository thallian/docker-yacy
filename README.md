Custom Websearch with [Yacy](http://yacy.net).

# Volumes
- /var/lib/yacy/DATA

# Environment Variables
## YACY_PASSWORD

The password for the admin user of the yacy admin interface.

# Ports
- 8090

# Capabilities
- CHOWN
- DAC_OVERRIDE
- NET_BIND_SERVICE
- SETGID
- SETUID
